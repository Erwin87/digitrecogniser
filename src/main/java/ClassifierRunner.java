import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClassifierRunner {

    public static void main(String[] args) {

        List<String> trainingsampleList = new ArrayList<>();

        try {
            Path pathTrainingsample = Paths.get("trainingsample.csv");
            trainingsampleList.addAll(Files.readAllLines(pathTrainingsample));
            trainingsampleList = trainingsampleList.subList(1, trainingsampleList.size());
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String[]> trainingsampleListAsArrays = trainingsampleList
                .stream()
                .map(x -> x.split(","))
                .collect(Collectors.toList());

        List<List<Integer>> trainingsampleListAsIntegers = trainingsampleListAsArrays
                .stream()
                .map(x -> {
                    return Arrays.stream(x)
                            .map(y -> Integer.parseInt(String.valueOf(y)))
                            .collect(Collectors.toList());
                })
                .collect(Collectors.toList());

        List<Record> trainingsampleListAsRecords = new ArrayList<Record>();
        for (List<Integer> element : trainingsampleListAsIntegers) {
            trainingsampleListAsRecords.add(new Record(element));
        }

        List<String> validationsampleList = new ArrayList<>();

        try {
            Path pathTrainingsample = Paths.get("validationsample.csv");
            validationsampleList.addAll(Files.readAllLines(pathTrainingsample));
            validationsampleList = validationsampleList.subList(1, validationsampleList.size());
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String[]> validationsampleListAsArrays = validationsampleList
                .stream()
                .map(x -> x.split(","))
                .collect(Collectors.toList());

        List<List<Integer>> validationsampleListAsIntegers = validationsampleListAsArrays
                .stream()
                .map(x -> {
                    return Arrays.stream(x)
                            .map(y -> Integer.parseInt(String.valueOf(y)))
                            .collect(Collectors.toList());
                })
                .collect(Collectors.toList());

        List<Record> validationsampleListAsRecords = new ArrayList<Record>();
        for (List<Integer> element : validationsampleListAsIntegers) {
            validationsampleListAsRecords.add(new Record(element));
        }

        Record minRecord = trainingsampleListAsRecords.get(0);

        for (int i = 0; i < validationsampleListAsRecords.size(); i++) {
            double minDistance = distance(validationsampleListAsRecords.get(i).patternAsArray, minRecord.patternAsArray);
            for (Record record : trainingsampleListAsRecords) {
                double recordDistance = distance(record.patternAsArray, validationsampleListAsRecords.get(i).patternAsArray);
                if (recordDistance < minDistance) {
                    minRecord = record;
                    minDistance = recordDistance;
                }
                System.out.println("VALIDATION DIGIT:" + validationsampleListAsRecords.get(i).digit);
                System.out.println("MIN DISTANCE DIGIT:" + minRecord.digit);
            }
        }
    }

    public static double distance(Integer[] a, Integer[] b) {
        double sum = 0;
        for (int x = 0; x < 28 * 28; x++) {
            sum += Math.pow(a[x] - b[x], 2);
        }
        return Math.sqrt(sum);
    }


}


