import java.util.List;

public class Record {

    public int digit;

    public Integer[] patternAsArray = new Integer[28 * 28];

    public Record(List<Integer> pattern) {
        digit = pattern.get(0);
        pattern.subList(1,pattern.size()).toArray(patternAsArray);
    }
}
